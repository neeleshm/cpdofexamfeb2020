package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;

public class AboutCPDOFTest {
@Test
  public void testdesc() throws Exception {
    String  abtStr   = new AboutCPDOF().desc();
    assertTrue(abtStr.contains("CP-DOF certification program covers end to end DevOps Life Cycle practically"));
}
}
