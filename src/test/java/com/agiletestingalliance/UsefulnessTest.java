package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;

public class UsefulnessTest {
@Test
  public void testdesc() throws Exception {
    String  abtStr   = new Usefulness().desc();
    assertTrue(abtStr.contains("DevOps is about transformation, about building quality in,"));
}

  @Test
  public void testfunctionWhile() throws Exception {
    Usefulness usef = new Usefulness();
    usef.functionWhile();
    assertEquals("IncrValue",4,4);
}
}
